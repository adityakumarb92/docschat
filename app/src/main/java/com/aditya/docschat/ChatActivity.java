package com.aditya.docschat;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.aditya.docschat.adapter.MessageAdapter;
import com.aditya.docschat.commonClasses.TinyDB;
import com.aditya.docschat.models.ChatResponse;
import com.aditya.docschat.models.MessageIdentifier;
import com.aditya.docschat.retrofit.ApiClient;
import com.aditya.docschat.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    EditText message;
    private static ApiInterface apiService;
    static List<MessageIdentifier> messageIdentifierList = new ArrayList<>();
    static List<MessageIdentifier> offlineMessageIdentifierList = new ArrayList<>();
    RecyclerView messageView;
    private MessageAdapter adapter;
    private SharedPreferences chatHistory;
    private static SharedPreferences.Editor editorDetails;
    private static int chatsize = 0;
    private SharedPreferences chatIdentity;
    private static SharedPreferences.Editor editorIdentity;
    private LinearLayoutManager layoutManager;
    private static TinyDB tinyDB;
    private static ArrayList<Object> objectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initialize();

    }

    private void initialize() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        tinyDB=new TinyDB(this);
        objectList=tinyDB.getListObject("offlineMessages",MessageIdentifier.class);
        for(Object object:objectList){
            offlineMessageIdentifierList.add((MessageIdentifier) object);
        }
        chatHistory = getSharedPreferences(Constants.sharedPrefChat, MODE_PRIVATE);
        chatIdentity = getSharedPreferences(Constants.sharedPrefChatIdentity, MODE_PRIVATE);
        chatsize = chatHistory.getInt(Constants.SIZE, 0);
        for (int i = 0; i < chatsize; i++) {
            MessageIdentifier messageIdentifier = new MessageIdentifier();
            messageIdentifier.setMessage(chatHistory.getString(String.valueOf(i), ""));
            messageIdentifier.setMe(chatIdentity.getBoolean(String.valueOf(i), true));
            messageIdentifierList.add(messageIdentifier);
        }
        editorDetails = chatHistory.edit();
        editorIdentity = chatIdentity.edit();
        message = findViewById(R.id.message);
        messageView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
//        layoutManager.setStackFromEnd(true);
        messageView.setLayoutManager(layoutManager);
        adapter = new MessageAdapter(this, messageIdentifierList);
        messageView.setAdapter(adapter);
        messageView.scrollToPosition(adapter.getItemCount() - 1);
    }

    public void sendMessage(View view) {
        MessageIdentifier messageIdentifier = new MessageIdentifier();
        messageIdentifier.setMe(true);
        String messageText = message.getText().toString().trim();
        messageIdentifier.setMessage(messageText);
        messageIdentifierList.add(messageIdentifier);
        editorDetails.putString(String.valueOf(chatsize), messageText);
        editorIdentity.putBoolean(String.valueOf(chatsize), true);
        chatsize++;
        editorDetails.putInt(Constants.SIZE, chatsize);
        editorIdentity.commit();
        editorDetails.commit();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setStackFromEnd(true);
        messageView.setLayoutManager(layoutManager);
        messageView.scrollToPosition(adapter.getItemCount() - 1);
        message.setText("");
        Constants.hideKeyboard(this, message);
        if (isNetworkAvailable()) {
            Call<ChatResponse> call = apiService.sendMessage(Constants.API_KEY, messageText, Constants.CHAT_BOT_ID, Constants.EXTERNAL_ID, "Aditya", "Kumar", "m");

            call.enqueue(new Callback<ChatResponse>() {
                @Override
                public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                    Log.d("message", response.body().getSuccess() + "");
                    if (response.body().getSuccess() == 1) {
                        MessageIdentifier messageIdentifier = new MessageIdentifier();
                        messageIdentifier.setMe(false);
                        messageIdentifier.setMessage(response.body().getMessage().getMessage());
                        messageIdentifierList.add(messageIdentifier);
                        editorDetails.putString(String.valueOf(chatsize), response.body().getMessage().getMessage());
                        editorIdentity.putBoolean(String.valueOf(chatsize), false);
                        chatsize++;
                        editorDetails.putInt(Constants.SIZE, chatsize);
                        editorIdentity.commit();
                        editorDetails.commit();
                    } else {
                        MessageIdentifier messageIdentifier = new MessageIdentifier();
                        messageIdentifier.setMe(false);
                        messageIdentifier.setMessage(response.body().getErrorMessage());
                        messageIdentifierList.add(messageIdentifier);
                        editorDetails.putString(String.valueOf(chatsize), response.body().getErrorMessage());
                        editorIdentity.putBoolean(String.valueOf(chatsize), false);
                        chatsize++;
                        editorDetails.putInt(Constants.SIZE, chatsize);
                        editorIdentity.commit();
                        editorDetails.commit();
                    }
                    LinearLayoutManager layoutManager = new LinearLayoutManager(ChatActivity.this);
//                layoutManager.setStackFromEnd(true);
                    messageView.setLayoutManager(layoutManager);
                    messageView.scrollToPosition(adapter.getItemCount() - 1);

                }

                @Override
                public void onFailure(Call<ChatResponse> call, Throwable t) {
                    MessageIdentifier messageIdentifier = new MessageIdentifier();
                    messageIdentifier.setMe(false);
                    messageIdentifier.setMessage("Error!");
                    messageIdentifierList.add(messageIdentifier);
                    editorDetails.putString(String.valueOf(chatsize), "Error!");
                    editorIdentity.putBoolean(String.valueOf(chatsize), false);
                    chatsize++;
                    editorDetails.putInt(Constants.SIZE, chatsize);
                    editorIdentity.commit();
                    editorDetails.commit();
                }
            });
        } else {
            offlineMessageIdentifierList.add(messageIdentifier);
            objectList.add(messageIdentifier);
            tinyDB.putListObject("offlineMessages",objectList);
        }
    }

    public static void sendOffline() {
        for (MessageIdentifier messageIdentifier : offlineMessageIdentifierList) {
            Call<ChatResponse> call = apiService.sendMessage(Constants.API_KEY, messageIdentifier.getMessage(), Constants.CHAT_BOT_ID, Constants.EXTERNAL_ID, "Aditya", "Kumar", "m");

            call.enqueue(new Callback<ChatResponse>() {
                @Override
                public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                    Log.d("message", response.body().getSuccess() + "");
                    if (response.body().getSuccess() == 1) {
                        MessageIdentifier messageIdentifier = new MessageIdentifier();
                        messageIdentifier.setMe(false);
                        messageIdentifier.setMessage(response.body().getMessage().getMessage());
                        messageIdentifierList.add(messageIdentifier);
                        editorDetails.putString(String.valueOf(chatsize), response.body().getMessage().getMessage());
                        editorIdentity.putBoolean(String.valueOf(chatsize), false);
                        chatsize++;
                        editorDetails.putInt(Constants.SIZE, chatsize);
                        editorIdentity.commit();
                        editorDetails.commit();
                    } else {
                        MessageIdentifier messageIdentifier = new MessageIdentifier();
                        messageIdentifier.setMe(false);
                        messageIdentifier.setMessage(response.body().getErrorMessage());
                        messageIdentifierList.add(messageIdentifier);
                        editorDetails.putString(String.valueOf(chatsize), response.body().getErrorMessage());
                        editorIdentity.putBoolean(String.valueOf(chatsize), false);
                        chatsize++;
                        editorDetails.putInt(Constants.SIZE, chatsize);
                        editorIdentity.commit();
                        editorDetails.commit();
                    }


                }

                @Override
                public void onFailure(Call<ChatResponse> call, Throwable t) {
                    MessageIdentifier messageIdentifier = new MessageIdentifier();
                    messageIdentifier.setMe(false);
                    messageIdentifier.setMessage("Error!");
                    messageIdentifierList.add(messageIdentifier);
                    editorDetails.putString(String.valueOf(chatsize), "Error!");
                    editorIdentity.putBoolean(String.valueOf(chatsize), false);
                    chatsize++;
                    editorDetails.putInt(Constants.SIZE, chatsize);
                    editorIdentity.commit();
                    editorDetails.commit();
                }
            });
        }

        offlineMessageIdentifierList=new ArrayList<>();
        objectList=new ArrayList<>();
        tinyDB.putListObject("offlineMessages",objectList);

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}