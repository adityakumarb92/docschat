package com.aditya.docschat;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;



public class Constants {
    public static final String SIZE = "size";
    public static String API_KEY = "JIK5fB6JyfOuAEWD";
    public static int CHAT_BOT_ID = 56061;
    public static String EXTERNAL_ID = "abcdef-123";
    public static final String sharedPrefChat = "chatHistory";//chat history
    public static String sharedPrefChatIdentity="chatIdentity";

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
