package com.aditya.docschat.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aditya.docschat.R;
import com.aditya.docschat.models.MessageIdentifier;

import java.util.List;



public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private Context context;
    private List<MessageIdentifier> messagesItems;

    public MessageAdapter(Context ctx, List<MessageIdentifier> messagesItems) {
        this.context = ctx;
        this.messagesItems = messagesItems;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
         if (messagesItems.get(viewType).isMe()) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_message_right, parent, false);

        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_message_left, parent, false);

        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtMsg.setText(messagesItems.get(position).getMessage());
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return messagesItems.size();

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtMsg;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtMsg = itemView.findViewById(R.id.txtMsg);
        }
    }
}
