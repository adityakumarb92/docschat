package com.aditya.docschat.models;

import com.google.gson.annotations.SerializedName;



public class ChatResponse {
    @SerializedName("success")
    private Integer success;
    @SerializedName("errorMessage")
    private String errorMessage;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public MessageBot getMessage() {
        return message;
    }

    public void setMessage(MessageBot message) {
        this.message = message;
    }

    @SerializedName("message")
    private MessageBot message;


}
