package com.aditya.docschat.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kings on 13/1/18.
 */

public class MessageBot {
    @SerializedName("chatBotName")
    private String chatBotName;
    @SerializedName("chatBotID")
    private Integer chatBotID;

    public String getChatBotName() {
        return chatBotName;
    }

    public void setChatBotName(String chatBotName) {
        this.chatBotName = chatBotName;
    }

    public Integer getChatBotID() {
        return chatBotID;
    }

    public void setChatBotID(Integer chatBotID) {
        this.chatBotID = chatBotID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    @SerializedName("message")
    private String message;
    @SerializedName("emotion")
    private String emotion;
}
