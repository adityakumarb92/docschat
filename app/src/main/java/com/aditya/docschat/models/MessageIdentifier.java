package com.aditya.docschat.models;

/**
 * Created by kings on 13/1/18.
 */

public class MessageIdentifier {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean me) {
        isMe = me;
    }

    private boolean isMe;
}
