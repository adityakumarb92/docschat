package com.aditya.docschat.retrofit;

import com.aditya.docschat.models.ChatResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by kings on 13/1/18.
 */

public interface ApiInterface {
    @GET(".")
    Call<ChatResponse> sendMessage(@Query("apiKey") String apiKey, @Query("message") String message, @Query("chatBotID") Integer chatBotId, @Query("externalID") String externalID, @Query("firstName") String firstName, @Query("lastName") String lastName, @Query("gender") String gender);


}
